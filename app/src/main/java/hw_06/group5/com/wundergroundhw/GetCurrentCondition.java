package hw_06.group5.com.wundergroundhw;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Pawan on 3/18/2016.
 */
public class GetCurrentCondition extends AsyncTask<String,Void,String> {

    Context mainActivity;
    DatabaseDataManager dm;
    ProgressDialog progressDialog;

    public GetCurrentCondition(Context context){
        this.mainActivity = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            int statusCode = connection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream in = connection.getInputStream();
                return GetConditionUtil.parseforecast.parseCurrentTemp(in);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(mainActivity);
        progressDialog.setMessage("Please Wait ...");
        progressDialog.show();
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d("hhh", s);
        dm = new DatabaseDataManager(mainActivity);
        if (s != null){
            Cities cities = dm.getcityByName(GlobalVariables.maincityname, GlobalVariables.mainstate);
            Log.d("pawanup", new Cities(cities.get_citykey(), cities.getCityname(), cities.getState(), s).toString());
            dm.UpdateCity(new Cities(cities.get_citykey(), cities.getCityname(), cities.getState(), s));
            Log.d("pawanupdated", dm.getAllCity().toString());
        }
        progressDialog.dismiss();
    }
}
