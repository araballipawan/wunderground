package hw_06.group5.com.wundergroundhw;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

public class WeatherDetailActivity extends AppCompatActivity implements Serializable {
    TextView location;
    ImageView imageView;
    TextView feelsLike,humidity,dewPoint,pressure,clouds,winds;
    TextView currentTemp,condition,maxTemp,minTemp;
    String locationPassed,i;
    ArrayList<HourlyWeather> forecastList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        setTitle("Details Activity");

        location = (TextView) findViewById(R.id.textView_location);
        imageView = (ImageView) findViewById(R.id.imageView);
        currentTemp = (TextView) findViewById(R.id.textView_CurrentTemp);
        condition = (TextView) findViewById(R.id.textView_condition);
        maxTemp = (TextView) findViewById(R.id.textView_MaxTemp);
        minTemp = (TextView) findViewById(R.id.textView_MinTemp);
        feelsLike = (TextView) findViewById(R.id.textView_FeelsLike);
        humidity = (TextView) findViewById(R.id.textView_humidity);
        dewPoint = (TextView) findViewById(R.id.textView_dewPoint);
        pressure = (TextView) findViewById(R.id.textView_Pressure);
        clouds = (TextView) findViewById(R.id.textView_Clouds);
        winds = (TextView) findViewById(R.id.textView_winds);
        ImageButton previousButton = (ImageButton) findViewById(R.id.imageButton_previous);
        ImageButton nextButton = (ImageButton) findViewById(R.id.imageButton_next);


        if (getIntent().getExtras() != null){
            locationPassed = getIntent().getExtras().getString(GetDataHourlyWeather.CLICKED_LOC);
            i = (String) getIntent().getExtras().getSerializable("Position");
        }

        forecastList = GlobalVariables.Globalforecast;

        new GetImage().execute(forecastList.get(Integer.parseInt(i)).getIconUrl());
        Log.d("test2", forecastList.size() + "");
        Log.d("test2", i + "");


        location.setText("Current Location:" + locationPassed + "(" + forecastList.get(Integer.parseInt(i)).getTime() + ")");
        currentTemp.setText(forecastList.get(Integer.parseInt(i)).getTemperature() + "°F");
        condition.setText(forecastList.get(Integer.parseInt(i)).getClouds());
        maxTemp.setText("Max Temperature: "+"\t"+forecastList.get(Integer.parseInt(i)).getMaximumTemp() + " Fahrenheit");
        minTemp.setText("Min Temperature: "+"\t" +forecastList.get(Integer.parseInt(i)).getMinimumTemp() +" Fahrenheit");
        feelsLike.setText("Feels Like: "+"\t" +forecastList.get(Integer.parseInt(i)).getFeelsLike() +" Fahrenheit");
        humidity.setText("Humidity: "+"\t" +forecastList.get(Integer.parseInt(i)).getHumidity() +"%");
        dewPoint.setText("Dewpoint: "+"\t" +forecastList.get(Integer.parseInt(i)).getDewpoint() +" Fahrenheit");
        pressure.setText("Pressure: "+"\t" +forecastList.get(Integer.parseInt(i)).getPressure() +" hPa");
        clouds.setText("Clouds: " + "\t" + forecastList.get(Integer.parseInt(i)).getClimateType());
        winds.setText("Winds: " + "\t" + forecastList.get(Integer.parseInt(i)).getWindSpeed() + " mph," + "" + forecastList.get(Integer.parseInt(i)).getWindDirection());

        final ListIterator iterator = forecastList.listIterator();

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(iterator.hasPrevious()){
                    HourlyWeather weather = (HourlyWeather) iterator.previous();
                    location.setText("Current Location:" + locationPassed + "(" + weather.getTime() + ")");
                    currentTemp.setText(weather.getTemperature() + "°F");
                    condition.setText(weather.getClouds());
                    maxTemp.setText("Max Temperature: "+"\t"+weather.getMaximumTemp() + " Fahrenheit");
                    minTemp.setText("Min Temperature: "+"\t" +weather.getMinimumTemp() +" Fahrenheit");
                    feelsLike.setText("Feels Like: "+"\t" +weather.getFeelsLike() +" Fahrenheit");
                    humidity.setText("Humidity: "+"\t" +weather.getHumidity() +"%");
                    dewPoint.setText("Dewpoint: "+"\t" +weather.getDewpoint() +" Fahrenheit");
                    pressure.setText("Pressure: "+"\t" +weather.getPressure() +" hPa");
                    clouds.setText("Clouds: " + "\t" + weather.getClimateType());
                    winds.setText("Winds: " + "\t" + weather.getWindSpeed() + " mph," + "" + weather.getWindDirection());
                }
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(iterator.hasNext()){
                    HourlyWeather weather = (HourlyWeather) iterator.previous();
                    location.setText("Current Location:" + locationPassed + "(" + weather.getTime() + ")");
                    currentTemp.setText(weather.getTemperature() + "°F");
                    condition.setText(weather.getClouds());
                    maxTemp.setText("Max Temperature: "+"\t"+weather.getMaximumTemp() + " Fahrenheit");
                    minTemp.setText("Min Temperature: "+"\t" +weather.getMinimumTemp() +" Fahrenheit");
                    feelsLike.setText("Feels Like: "+"\t" +weather.getFeelsLike() +" Fahrenheit");
                    humidity.setText("Humidity: "+"\t" +weather.getHumidity() +"%");
                    dewPoint.setText("Dewpoint: "+"\t" +weather.getDewpoint() +" Fahrenheit");
                    pressure.setText("Pressure: "+"\t" +weather.getPressure() +" hPa");
                    clouds.setText("Clouds: " + "\t" + weather.getClimateType());
                    winds.setText("Winds: " + "\t" + weather.getWindSpeed() + " mph," + "" + weather.getWindDirection());
                }
            }
        });
    }

    //ASYNC task to get images
    class GetImage extends AsyncTask<String, Void, Bitmap> {
        Bitmap image = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(image);
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            InputStream inputStream = null;
            String urlDisplay = urls[0];
            try {
                inputStream = new java.net.URL(urlDisplay).openStream();
                image = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return image;
        }

    }
}
