package hw_06.group5.com.wundergroundhw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class HourlyData extends AppCompatActivity {
    private static String url = "http://api.wunderground.com/api/80505bfbb77ef2bd/hourly/q/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hourly_data);

        TextView location = (TextView) findViewById(R.id.textView_Hourlycurr);
        location.setText("Current Location:" + CityData.city_state.getCityname() + "," + CityData.city_state.getState());

        String finalurl = url +  CityData.city_state.getState() + "/" + CityData.city_state.getCityname() + ".xml";
        Log.d("url", finalurl);

        new GetDataHourlyWeather(this).execute(finalurl);
    }
}
