package hw_06.group5.com.wundergroundhw;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pawan on 3/18/2016.
 */
public class NotesAdapter extends ArrayAdapter<Notes> {

    Context mContext;
    int mResource;
    List<Notes> mData;

    public NotesAdapter(Context context, int resource, List<Notes> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mData = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            //inflate new view
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource,parent,false);
        }

        Notes notes = mData.get(position);

        TextView note = (TextView) convertView.findViewById(R.id.textView_location);
        note.setText(notes.getNote());

        TextView curr = (TextView) convertView.findViewById(R.id.textView_CurrentWeather);
        curr.setText(notes.getDate());
        return convertView;
    }
}
