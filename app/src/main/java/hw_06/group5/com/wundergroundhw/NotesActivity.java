package hw_06.group5.com.wundergroundhw;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

public class NotesActivity extends AppCompatActivity {

    ListView listView;
    NotesAdapter adapter;
    DatabaseDataManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        dm = new DatabaseDataManager(this);

        Log.d("pawan", dm.getAllNote().toString());

        adapter = new NotesAdapter(this,R.layout.row_item_layout,dm.getAllNote());

        listView = (ListView) findViewById(R.id.listView_NotesView);

        listView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        dm.close();
        super.onDestroy();
    }
}
