package hw_06.group5.com.wundergroundhw;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Pawan on 3/18/2016.
 */
public class GetDataDayWeather extends AsyncTask<String,Void,ArrayList<ForecastDay>> {

    ProgressDialog progressDialog;
    IData forecastActivity;
    ForecastDayAdapter adapter;
    ListView listView;

    public GetDataDayWeather(IData forecastActivity) {
        this.forecastActivity = forecastActivity;
    }

    @Override
    protected ArrayList<ForecastDay> doInBackground(String... params) {
        Log.d("demo", "Inside do in background");
        try {
            Log.d("demo","enter try");
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            Log.d("demo",url.toString());
            int statusCode = connection.getResponseCode();
            Log.d("demo", "Status code" + String.valueOf(statusCode));
            if (statusCode == HttpURLConnection.HTTP_OK) {
                Log.d("demo","Inside http ok");
                InputStream in = connection.getInputStream();
                return GetDataDayUtil.PullParseForecast.parseforecast.parseDayData(in);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(forecastActivity.getContext());
        progressDialog.setMessage("Loading Day Data");
        progressDialog.show();
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPostExecute(final ArrayList<ForecastDay> forecastDays) {
        super.onPostExecute(forecastDays);
/*        if(forecastDays != null) {
            Log.d("demo", "Data" + forecastDays.get(1).toString());
        }*/
        GlobalVariables.forcastdayss = forecastDays;
        forecastActivity.SetupData(forecastDays);
        progressDialog.dismiss();
    }

    static public interface IData{
        public void SetupData(ArrayList<ForecastDay> forecastDays);
        public Context getContext();
    }
}
