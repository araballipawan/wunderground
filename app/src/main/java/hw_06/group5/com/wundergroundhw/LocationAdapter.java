package hw_06.group5.com.wundergroundhw;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.List;

/**
 * Created by Pawan on 3/17/2016.
 */
public class LocationAdapter extends ArrayAdapter<Cities> {

    Context mContext;
    int mResource;
    List<Cities> mData;

    public LocationAdapter(Context context, int resource, List<Cities> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mData = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            //inflate new view
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource,parent,false);
        }

        Cities cities = mData.get(position);
        TextView city_state = (TextView) convertView.findViewById(R.id.textView_location);
        city_state.setText(cities.getCityname() + ", " + cities.getState());

        TextView curr = (TextView) convertView.findViewById(R.id.textView_CurrentWeather);
        curr.setText(cities.getCurrWeather());

        return convertView;
    }
}
