package hw_06.group5.com.wundergroundhw;

/**
 * Created by Pawan on 3/19/2016.
 */
public class ForecastDay {

    String date,highTemp,lowTemp,clouds, iconUrl, maxwindSpeed, windDirection, avghumidity;
    int indicator;

    public ForecastDay() {
    }

    public int getIndicator() {

        return indicator;
    }

    @Override
    public String toString() {
        return "ForecastDay{" +
                "date='" + date + '\'' +
                ", highTemp='" + highTemp + '\'' +
                ", lowTemp='" + lowTemp + '\'' +
                ", clouds='" + clouds + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", maxwindSpeed='" + maxwindSpeed + '\'' +
                ", windDirection='" + windDirection + '\'' +
                ", avghumidity='" + avghumidity + '\'' +
                ", indicator=" + indicator +
                '}';
    }

    public void setIndicator(int indicator) {
        this.indicator = indicator;
    }

    public ForecastDay(String date, String highTemp, String lowTemp, String clouds, String iconUrl, String maxwindSpeed, String windDirection, String avghumidity, int indicator) {
        this.date = date;
        this.highTemp = highTemp;
        this.lowTemp = lowTemp;
        this.clouds = clouds;
        this.iconUrl = iconUrl;
        this.maxwindSpeed = maxwindSpeed;
        this.windDirection = windDirection;
        this.avghumidity = avghumidity;
        this.indicator = indicator;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHighTemp() {
        return highTemp;
    }

    public void setHighTemp(String highTemp) {
        this.highTemp = highTemp;
    }

    public String getLowTemp() {
        return lowTemp;
    }

    public void setLowTemp(String lowTemp) {
        this.lowTemp = lowTemp;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getMaxwindSpeed() {
        return maxwindSpeed;
    }

    public void setMaxwindSpeed(String maxwindSpeed) {
        this.maxwindSpeed = maxwindSpeed;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public String getAvghumidity() {
        return avghumidity;
    }

    public void setAvghumidity(String avghumidity) {
        this.avghumidity = avghumidity;
    }
}
