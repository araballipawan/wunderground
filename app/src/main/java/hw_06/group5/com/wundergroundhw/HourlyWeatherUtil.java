package hw_06.group5.com.wundergroundhw;

import android.util.Log;

import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Pawan on 3/18/2016.
 */
public class HourlyWeatherUtil {

    static public class parseforecast extends DefaultHandler {

        static ArrayList<HourlyWeather> parseNews(InputStream in) throws XmlPullParserException, IOException {
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(in, "UTF-8");
            HourlyWeather HourlyWeather = null;
            ArrayList<HourlyWeather> HourlyWeatherList = new ArrayList<HourlyWeather>();
            int event = parser.getEventType();
            String direction = null;
            while (event != XmlPullParser.END_DOCUMENT) {
                switch (event) {

                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals("forecast")) {
                            HourlyWeather = new HourlyWeather();
                        } else if (parser.getName().equals("civil")) {
                            HourlyWeather.setTime(parser.nextText().trim());
                        } else if (parser.getName().equals("temp")) {
                            int next = parser.nextTag();
                            if (next == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("english"))
                                    HourlyWeather.setTemperature(parser.nextText().trim());
                            }
                        } else if (parser.getName().equals("dewpoint")) {
                            int next = parser.nextTag();
                            if (next == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("english"))
                                    HourlyWeather.setDewpoint(parser.nextText().trim());
                            }
                        } else if (parser.getName().equals("icon_url")) {
                            HourlyWeather.setIconUrl(parser.nextText().trim());
                        } else if (parser.getName().equals("wspd")) {
                            int next = parser.nextTag();
                            if (next == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("english"))
                                    HourlyWeather.setWindSpeed(parser.nextText().trim());
                            }
                        } else if (parser.getName().equals("dir")) {
                            direction = parser.nextText().trim();
                        } else if (parser.getName().equals("degrees")) {
                            HourlyWeather.setWindDirection(parser.nextText().trim() + " " + direction);
                        } else if (parser.getName().equals("wx")) {
                            HourlyWeather.setClimateType(parser.nextText().trim());
                        } else if (parser.getName().equals("humidity")) {
                            HourlyWeather.setHumidity(parser.nextText().trim());
                        } else if (parser.getName().equals("feelslike")) {
                            int next = parser.nextTag();
                            if (next == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("english"))
                                    HourlyWeather.setFeelsLike(parser.nextText().trim());
                            }
                        } else if (parser.getName().equals("mslp")) {
                            int next = parser.nextTag();
                            if (next == XmlPullParser.START_TAG) {
                                if (parser.getName().equals("english"))
                                    HourlyWeather.setPressure(parser.nextText().trim());
                            }
                        } else if (parser.getName().equals("condition")) {
                            HourlyWeather.setClouds(parser.nextText().trim());
                        }


                        break;
                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals("forecast"))
                            HourlyWeatherList.add(HourlyWeather);
                        break;
                    default:
                        break;
                }
                event = parser.next();
            }
            return HourlyWeatherList;
        }
    }
}

