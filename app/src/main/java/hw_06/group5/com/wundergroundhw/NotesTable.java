package hw_06.group5.com.wundergroundhw;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Pawan on 3/16/2016.
 */
public class NotesTable {

    static final String TABLE_NAME = "notes";
    static final String COLUMN_NOTEKEY = "_notekey";
    static final String COLUMN_NOTEDATE = "date";
    static final String COLUMN_NOTE = "note";

    public static void onCreate(SQLiteDatabase db) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("CREATE TABLE " + TABLE_NAME + " ( ");
        stringBuilder.append(COLUMN_NOTEKEY + " REFERENCES cities(_citykey) ON UPDATE CASCADE, ");
        stringBuilder.append(COLUMN_NOTEDATE + " text not null , ");
        stringBuilder.append(COLUMN_NOTE + " text not null ); ");
        try {
            db.execSQL(stringBuilder.toString());
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS" + TABLE_NAME);
        NotesTable.onCreate(db);
    }
}
