package hw_06.group5.com.wundergroundhw;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView display;
    ListView listView;

    static final String LOCATION_KEY = "location";
    LocationAdapter adapter;
    DatabaseDataManager dm;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dm = new DatabaseDataManager(this);

        Log.d("pawan", dm.getAllNote().toString());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("In Progress...");
        progressDialog.setCancelable(false);

        List<Cities> dbData = new ArrayList<>();

        Log.d("pawan",dm.getAllCity().toString());

        for (int i = 0; i <dm.getAllCity().size() ; i++) {
            GlobalVariables.maincityname = dm.getAllCity().get(i).getCityname();
            GlobalVariables.mainstate = dm.getAllCity().get(i).getState();
            new GetCurrentCondition(this).execute("http://api.wunderground.com/api/80505bfbb77ef2bd/conditions/q/"+GlobalVariables.mainstate+"/"+GlobalVariables.maincityname+".xml");
        }

        display = (TextView) findViewById(R.id.textView_main);
        adapter = new LocationAdapter(this,R.layout.row_item_layout,dm.getAllCity());
        adapter.setNotifyOnChange(true);
        listView = (ListView) findViewById(R.id.listView_cities);

        if (GlobalVariables.counter == 0){
            progressDialog.show();
            readAllCitiesAndState();
            GlobalVariables.counter++;
            progressDialog.dismiss();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), CityData.class);
                intent.putExtra(LOCATION_KEY, dm.getAllCity().get(position));
                startActivity(intent);
                finish();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                // TODO Auto-generated method stub
                //need to delete city
                dm.deleteCity(dm.getAllCity().get(position));
                adapter.notifyDataSetChanged();
                return true;
            }
        });

        checkToDisplay();
    }

    public void checkToDisplay() {
        if (dm.getAllCity().size() > 0){
            listView.setAdapter(adapter);
            adapter.setNotifyOnChange(true);
            display.setText("");
        }
        else {
            display.setText("There are no cities to display. Add using the Add City from the menu");
        }
    }

    public void readAllCitiesAndState() {

        InputStream inputStream = getResources().openRawResource(R.raw.cities);
        CVSReader csv = new CVSReader(inputStream);
        List<String[]> scoreList = csv.read();

        for (int i = 0; i < scoreList.size(); i++) {
            if (i != 0) {
                String city = scoreList.get(i)[2];
                String state = scoreList.get(i)[1];
                GlobalVariables.allCitiesStateArrayList.add(city + "," + state);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_addcity:
                Intent inte = new Intent(MainActivity.this,AddCityActivity.class);
                startActivity(inte);
                break;
            // action with ID action_settings was selected
            case R.id.action_clearsavedcity:
                dm.deleteAllCity();
                adapter.setNotifyOnChange(true);
                break;
            case R.id.action_viewnotes:
                Intent intent = new Intent(MainActivity.this,NotesActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return true;
    }

    private boolean isConnectedOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        //networkInfo.getType()
        if(networkInfo != null && networkInfo.isConnected()){
            return true;
        }
        return false;
    }
}
