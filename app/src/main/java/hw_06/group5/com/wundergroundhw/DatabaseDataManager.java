package hw_06.group5.com.wundergroundhw;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

/**
 * Created by Pawan on 3/17/2016.
 */
public class DatabaseDataManager {
    private Context mContext;
    private DatabaseOpenHelper databaseOpenHelper;
    private SQLiteDatabase db;
    private NotesDAO notesDAO;
    private CitiesDAO citiesDAO;

    public DatabaseDataManager(Context context){
        this.mContext = context;
        databaseOpenHelper = new DatabaseOpenHelper(this.mContext);
        db = databaseOpenHelper.getWritableDatabase();
        notesDAO = new NotesDAO(db);
        citiesDAO = new CitiesDAO(db);
    }

    public void close(){
        if(db != null){
            db.close();
        }
    }

    public NotesDAO getNotesDAO() {
        return this.notesDAO;
    }

    public CitiesDAO getCitiesDAO() {
        return this.citiesDAO;
    }

    public long saveCity(Cities cities){
        return this.citiesDAO.save(cities);
    }

    public long saveNote(Notes notes){
        return this.notesDAO.save(notes);
    }

    public boolean deleteCity(Cities cities){
        return this.citiesDAO.delete(cities);
    }

    public boolean deleteNote(Notes notes){
        return this.notesDAO.delete(notes);
    }

    public Notes getNote(long _id){
        return this.notesDAO.get(_id);
    }

    public Cities getCity(long _id){
        return this.citiesDAO.get(_id);
    }

    public List<Notes> getAllNote(){
        return this.notesDAO.getAll();
    }

    public List<Cities> getAllCity(){
        return this.citiesDAO.getAllCities();
    }

    public boolean deleteAllCity() {return this.citiesDAO.deleteAll();}

    public Cities getcityByName(String city, String state){
        return this.citiesDAO.getCityWithName(city, state);
    }

    public boolean UpdateCity(Cities cities){
        return this.citiesDAO.update(cities);
    }
}
