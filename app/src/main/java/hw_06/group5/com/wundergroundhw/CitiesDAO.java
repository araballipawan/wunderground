package hw_06.group5.com.wundergroundhw;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawan on 3/17/2016.
 */
public class CitiesDAO {

    SQLiteDatabase db;

    public CitiesDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public long save(Cities cities){
        ContentValues values = new ContentValues();
        values.put(CitiesTable.COLUMN_CITYNAME,cities.getCityname());
        values.put(CitiesTable.COLUMN_STATE,cities.getState());

        return db.insert(CitiesTable.TABLE_NAME,null, values);
    }

    public boolean delete(Cities cities){
        return db.delete(CitiesTable.TABLE_NAME,CitiesTable.COLUMN_CITYKEY + " =? ",new String[]{cities.get_citykey() + ""})>0;
    }

    public boolean deleteAll(){
        return db.delete(CitiesTable.TABLE_NAME,null,null)>0;
    }

    public Cities get(long _citykey){
        Cities cities = null;
        Cursor c;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            c = db.query(true, CitiesTable.TABLE_NAME, new String[]{CitiesTable.COLUMN_CITYNAME, CitiesTable.COLUMN_STATE,CitiesTable.COLUMN_CURRWEATHER},
                    CitiesTable.COLUMN_CITYKEY + " =? ", new String[]{_citykey + ""}, null, null, null, null, null);
            if (c != null && c.moveToFirst()){
                cities = buildCityfromCursor(c);
                if (!c.isClosed()){
                    c.close();
                }
            }
        }
        return cities;
    }

    public List<Cities> getAllCities(){
        List<Cities> cities = new ArrayList<>();

        Cursor c = db.query(CitiesTable.TABLE_NAME,new String[]{CitiesTable.COLUMN_CITYKEY,CitiesTable.COLUMN_CITYNAME,CitiesTable.COLUMN_STATE,CitiesTable.COLUMN_CURRWEATHER},
                null,null,null,null,null);
        if (c != null && c.moveToFirst()){
            do {
                Cities city = buildCityfromCursor(c);
                if (city != null){
                    cities.add(city);
                }
            }while (c.moveToNext());
            if (!c.isClosed()){
                c.close();
            }
        }
        return cities;
    }

    public Cities getCityWithName(String city, String state){
        Cities cities = null;
        Cursor c;

        c = db.query(CitiesTable.TABLE_NAME,new String[]{CitiesTable.COLUMN_CITYKEY,CitiesTable.COLUMN_CITYNAME,CitiesTable.COLUMN_STATE,CitiesTable.COLUMN_CURRWEATHER},
                CitiesTable.COLUMN_CITYNAME + " =?" + " AND " + CitiesTable.COLUMN_STATE +" =?", new String[]{city,state},null,null,null);
        if (c != null && c.moveToFirst()){
            cities = buildCityfromCursor(c);
            if (!c.isClosed()){
                c.close();
            }
        }
        return cities;
    }

    public boolean update(Cities cities){
        ContentValues values = new ContentValues();
        values.put(CitiesTable.COLUMN_CITYNAME,cities.getCityname());
        values.put(CitiesTable.COLUMN_STATE, cities.getState());
        values.put(CitiesTable.COLUMN_CURRWEATHER, cities.getCurrWeather());
        //Log.d("pawanup",db.update(CitiesTable.TABLE_NAME, values, CitiesTable.COLUMN_CITYKEY + "=?", new String[]{cities.get_citykey() + ""})+"");
        return db.update(CitiesTable.TABLE_NAME, values, CitiesTable.COLUMN_CITYKEY + "=?", new String[]{cities.get_citykey() + ""})>0;
    }
    public Cities buildCityfromCursor(Cursor c) {
        Cities cities = null;
        if (c != null){
            cities = new Cities();
            cities.set_citykey(c.getLong(0));
            cities.setCityname(c.getString(1));
            cities.setState(c.getString(2));
        }
        return cities;
    }
}
