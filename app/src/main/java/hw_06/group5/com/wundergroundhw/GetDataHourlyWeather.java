package hw_06.group5.com.wundergroundhw;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Pawan on 3/18/2016.
 */
public class GetDataHourlyWeather extends AsyncTask<String,Void,ArrayList<HourlyWeather>> {

    ProgressDialog progressDialog;
    HourlyData hourlyData;

    final static String CLICKED_LOC = "CLICK_LOC";
    final static String CLICKED_TEMP = "CLICK_TEMP";
    final static String CLICKED_DIR = "CLICKED_DIR";
    final static String CLICKED_FEELS = "CLICKED_FEELS";
    final static String CLICKED_DEWPOINT = "CLICKED_DEWPOINT";
    final static String CLICKED_MSPL = "CLICKED_MSPL";
    final static String CLICKED_WSPD = "CLICKED_WSPD";

    public GetDataHourlyWeather(HourlyData hourlyData) {
        this.hourlyData = hourlyData;
    }

    @Override
    protected ArrayList<HourlyWeather> doInBackground(String... params) {
        Log.d("demo", "Inside do in background");
        try {
            Log.d("demo","enter try");
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            Log.d("demo",url.toString());
            int statusCode = connection.getResponseCode();
            Log.d("demo", "Status code" + String.valueOf(statusCode));
            if (statusCode == HttpURLConnection.HTTP_OK) {
                Log.d("demo","Inside http ok");
                InputStream in = connection.getInputStream();
                return HourlyWeatherUtil.parseforecast.parseNews(in);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(hourlyData);
        progressDialog.setMessage("Loading Hourly Data");
        progressDialog.show();
        progressDialog.setCancelable(false);
    }

    @Override
    protected void onPostExecute(final ArrayList<HourlyWeather> hourlyWeathers) {
        super.onPostExecute(hourlyWeathers);
        if(hourlyWeathers != null) {
            Log.d("demo", "Data" + hourlyWeathers.get(1).toString());
        }

        ListView listView = (ListView) hourlyData.findViewById(R.id.listViewHourly);
        WeatherAdapter adapter = new WeatherAdapter(hourlyData.getApplicationContext(),R.layout.rowitemlayout,hourlyWeathers);

        listView.setAdapter(adapter);
        GlobalVariables.Globalforecast = hourlyWeathers;

        adapter.setNotifyOnChange(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Log.d("this", "Position : " + position + "Value : " + GlobalVariables.Globalforecast.get(position).toString());
                Intent inte = new Intent(hourlyData, WeatherDetailActivity.class);
                inte.putExtra(CLICKED_WSPD, hourlyWeathers.get(position).getWindSpeed());
                inte.putExtra(CLICKED_LOC, CityData.city_state.getCityname() + "," + CityData.city_state.getState());
                inte.putExtra(CLICKED_TEMP, hourlyWeathers.get(position).getTemperature());
                inte.putExtra(CLICKED_DEWPOINT, hourlyWeathers.get(position).getDewpoint());
                inte.putExtra(CLICKED_FEELS, hourlyWeathers.get(position).getFeelsLike());
                inte.putExtra(CLICKED_MSPL, hourlyWeathers.get(position).getPressure());
                inte.putExtra(CLICKED_DIR, hourlyWeathers.get(position).getWindDirection());
                Bundle bundle = new Bundle();
                bundle.putSerializable("Position", String.valueOf(position));
                inte.putExtras(bundle);
                hourlyData.startActivity(inte);
            }
        });


        progressDialog.setMessage("Loading completed");
        progressDialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        }, 1000);  // 3000 milliseconds

    }

    public static int GetMax(int a,int b){
        return a>b?a:b;
    }

    public static int GetMin(int a, int b){
        return a<b?a:b;
    }
}
