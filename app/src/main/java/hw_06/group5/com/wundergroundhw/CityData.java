package hw_06.group5.com.wundergroundhw;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class CityData extends TabActivity {

    static Cities city_state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_data);

        if(getIntent().getExtras() != null){
            city_state = (Cities) getIntent().getExtras().getParcelable(MainActivity.LOCATION_KEY);
        }


        TabHost tabHost = getTabHost();

        // Tab for Photos
        TabHost.TabSpec hourlydata = tabHost.newTabSpec("HourlyData");
        // setting Title and Icon for the Tab
        hourlydata.setIndicator("Hourly Data");
        Intent hourlyintent = new Intent(this, HourlyData.class);
        hourlydata.setContent(hourlyintent);

        // Tab for Photos
        TabHost.TabSpec forecast = tabHost.newTabSpec("Forecast");
        // setting Title and Icon for the Tab
        forecast.setIndicator("Forecast");
        Intent photosIntent = new Intent(this, ForecastActivity.class);
        forecast.setContent(photosIntent);

        tabHost.addTab(hourlydata);
        tabHost.addTab(forecast);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
