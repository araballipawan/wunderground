package hw_06.group5.com.wundergroundhw;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Pawan on 3/17/2016.
 */
public class GlobalVariables implements Serializable {

    static int counter = 0;
    static ArrayList<String> allCitiesStateArrayList = new ArrayList<>();
    static ArrayList<Cities> citiesArrayList = new ArrayList<>();
    static ArrayList<HourlyWeather> Globalforecast = new ArrayList<HourlyWeather>();
    static int max = 0;
    static int min;
    static boolean indi = false;
    static String maincityname,mainstate;
    static ArrayList<ForecastDay> forcastdayss = new ArrayList<>();

    static boolean DontCall = false;
}
