package hw_06.group5.com.wundergroundhw;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

/**
 * Created by Pawan on 2/29/2016.
 */
public class WeatherAdapter extends ArrayAdapter<HourlyWeather> {
    List<HourlyWeather> mData;
    Context mContext;
    int mResource;
    public WeatherAdapter(Context context, int resource, List<HourlyWeather> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mData = objects;
        this.mResource = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource,parent,false);
        }

        final HourlyWeather fo = mData.get(position);

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder().header("Cache-Control", "max-age=" + (60 * 60 * 24 * 365)).build();
            }
        });

        okHttpClient.setCache(new Cache(mContext.getCacheDir(), Integer.MAX_VALUE));
        OkHttpDownloader okHttpDownloader = new OkHttpDownloader(okHttpClient);
        final Picasso picasso = new Picasso.Builder(mContext).downloader(okHttpDownloader).build();
        final View finalConvertView = convertView;
        picasso.load(fo.getIconUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into((ImageView) convertView.findViewById(R.id.imageView), new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Log.d("picaso", "I am here");
                        picasso.load(fo.getIconUrl()).into((ImageView) finalConvertView.findViewById(R.id.imageView));
            }
        });

        TextView timeTextView = (TextView) convertView.findViewById(R.id.textView_time);

        //Log.d("getcivic",fo.getMaximumTemp());

        timeTextView.setText(fo.getTime());

        TextView textViewType = (TextView) convertView.findViewById(R.id.textView_type);
        textViewType.setText(fo.getClimateType());

        TextView tempTextView = (TextView) convertView.findViewById(R.id.textView_temp);

        tempTextView.setText(fo.getTemperature() +"°F");
        //colorHexTextView.setTextColor(android.graphics.Color.parseColor(color.colorHex));

        return convertView;

    }
}
