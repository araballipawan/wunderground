package hw_06.group5.com.wundergroundhw;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawan on 3/17/2016.
 */
public class NotesDAO {

    SQLiteDatabase db;

    public NotesDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public long save(Notes notes){
        ContentValues values = new ContentValues();
        values.put(NotesTable.COLUMN_NOTEKEY,notes.get_notekey());
        values.put(NotesTable.COLUMN_NOTEDATE,notes.getDate());
        values.put(NotesTable.COLUMN_NOTE,notes.getNote());

        return db.insert(NotesTable.TABLE_NAME,null,values);
    }

    public boolean delete(Notes notes){
        return db.delete(NotesTable.TABLE_NAME,NotesTable.COLUMN_NOTEKEY + " =? ",new String[]{notes.get_notekey()+ ""})>0;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public Notes get(long _notekey){

        Notes notes = null;
        Cursor c;

        c = db.query(true,NotesTable.TABLE_NAME,new String[]{NotesTable.COLUMN_NOTEDATE,NotesTable.COLUMN_NOTE},
                NotesTable.COLUMN_NOTEKEY + " =? ", new String[]{_notekey +""},null,null,null,null,null);

        if ( c != null && c.moveToFirst()){
            notes = buildNoteFromCursor(c);
            if (!c.isClosed()){
                c.close();
            }
        }
        return notes;
    }

    public List<Notes> getAll(){
        List<Notes> notes = new ArrayList<>();

        Cursor c = db.query(NotesTable.TABLE_NAME,new String[]{NotesTable.COLUMN_NOTEKEY,NotesTable.COLUMN_NOTEKEY,NotesTable.COLUMN_NOTEDATE,
                        NotesTable.COLUMN_NOTE},
                null,null,null,null,null);
        if (c != null && c.moveToFirst()){
            do {
                Notes note = buildNoteFromCursor(c);
                if (note != null){
                    notes.add(note);
                }
            }while (c.moveToNext());
            if (!c.isClosed()){
                c.close();
            }
        }
        return notes;
    }

    public Notes buildNoteFromCursor(Cursor c) {
        Notes notes = null;
        if (c != null){
            notes = new Notes();
            notes.set_notekey(c.getLong(0));
            notes.setDate(c.getString(1));
            notes.setNote(c.getString(2));
        }
        return notes;
    }
}
