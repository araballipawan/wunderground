package hw_06.group5.com.wundergroundhw;

import android.util.Log;
import android.widget.Toast;

import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Pawan on 3/18/2016.
 */
public class GetConditionUtil {

    static public class parseforecast extends DefaultHandler {

        static String parseCurrentTemp(InputStream in) throws XmlPullParserException, IOException {
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(in, "UTF-8");
            int event = parser.getEventType();
            String currentTemp = null;
            MainActivity mainActivity;

            while (event != XmlPullParser.END_DOCUMENT) {
                switch (event) {
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals("temp_f")) {
                            currentTemp = parser.nextText().trim();
                            Log.d("currentTemp", currentTemp);
                            break;
                        }
                    default:
                        break;
                }
                event = parser.next();
            }
            return currentTemp;
        }
    }
}
