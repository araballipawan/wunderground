package hw_06.group5.com.wundergroundhw;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AddNotesActivity extends AppCompatActivity {

    EditText editText_Enternote;
    Button buttonSaveNote;
    DatabaseDataManager dm;
    String position,dat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);

        dm = new DatabaseDataManager(this);

        if(getIntent().getExtras() != null){
            position = getIntent().getExtras().getString(ForecastActivity.POSITION_KEY);
            dat = getIntent().getExtras().getString(ForecastActivity.DATE_KEY);
        }

        editText_Enternote = (EditText) findViewById(R.id.editText_note);
        buttonSaveNote = (Button) findViewById(R.id.button_savenote);


        buttonSaveNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText_Enternote.getText().length() >30){
                    editText_Enternote.setError("Enter character less than 30");
                }else{
                    Intent intent = new Intent(AddNotesActivity.this,ForecastActivity.class);
                    String note = editText_Enternote.getText().toString();
                    Log.d("pawan4",note);
                    String city = CityData.city_state.getCityname();
                    String state1 = CityData.city_state.getState();
                    Cities cities;
                    cities = dm.getcityByName(city,state1);
                    dm.saveNote(new Notes(cities.get_citykey(),dat,note));
                    //imageView.setVisibility(View.VISIBLE);
                    GlobalVariables.indi = true;
                    GlobalVariables.DontCall = true;
                    Log.d("pawan3",dm.getAllNote().toString());
                    startActivity(intent);
                }
            }
        });
    }
}
