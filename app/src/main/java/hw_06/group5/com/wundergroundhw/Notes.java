package hw_06.group5.com.wundergroundhw;

/**
 * Created by Pawan on 3/16/2016.
 */
public class Notes {

    private long _notekey;
    private String date, note;

    public Notes(long _notekey, String date, String note) {
        this._notekey = _notekey;
        this.date = date;
        this.note = note;
    }

    public Notes() {
    }

    @Override
    public String toString() {
        return "notes{" +
                "_notekey=" + _notekey +
                ", date='" + date + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public long get_notekey() {
        return _notekey;
    }

    public void set_notekey(long _notekey) {
        this._notekey = _notekey;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
