package hw_06.group5.com.wundergroundhw;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

/**
 * Created by Pawan on 3/19/2016.
 */
public class ForecastDayAdapter extends ArrayAdapter<ForecastDay>{

        List<ForecastDay> mData;
        Context mContext;
        int mResource;
    DatabaseDataManager dm;
        public ForecastDayAdapter(Context context, int resource, List<ForecastDay> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mData = objects;
            this.mResource = resource;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

dm = new DatabaseDataManager(mContext);
            if (convertView == null){
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource,parent,false);
            }

            final ForecastDay fo = mData.get(position);

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder().header("Cache-Control", "max-age=" + (60 * 60 * 24 * 365)).build();
                }
            });

            okHttpClient.setCache(new Cache(mContext.getCacheDir(), Integer.MAX_VALUE));
            OkHttpDownloader okHttpDownloader = new OkHttpDownloader(okHttpClient);
            final Picasso picasso = new Picasso.Builder(mContext).downloader(okHttpDownloader).build();
            final View finalConvertView = convertView;

                picasso.load(fo.getIconUrl())
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into((ImageView) convertView.findViewById(R.id.imageView_day), new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                Log.d("picaso", "I am here");
                                picasso.load(fo.getIconUrl()).into((ImageView) finalConvertView.findViewById(R.id.imageView_day));
                            }
                        });

            TextView timeTextView = (TextView) convertView.findViewById(R.id.textView_time_day);

            //Log.d("getcivic",fo.getMaximumTemp());

            timeTextView.setText(fo.getDate());

            TextView textViewType = (TextView) convertView.findViewById(R.id.textView_type_day);
            textViewType.setText(fo.getClouds());

            TextView tempTextView = (TextView) convertView.findViewById(R.id.textView_temp_day);

            tempTextView.setText(fo.getHighTemp() +"°F/");

            TextView textViewmin = (TextView) convertView.findViewById(R.id.textView_min_day);

            textViewmin.setText(fo.getLowTemp());

            ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView2);
            for (int i = 0; i <dm.getAllNote().size() ; i++) {
                if (fo.getDate().equals(dm.getAllNote().get(i).getDate())){
                    imageView.setVisibility(View.VISIBLE);
                }else{
                    imageView.setVisibility(View.INVISIBLE);
                }
            }

            //colorHexTextView.setTextColor(android.graphics.Color.parseColor(color.colorHex));

            return convertView;

        }
}
