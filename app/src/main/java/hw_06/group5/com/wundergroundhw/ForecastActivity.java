package hw_06.group5.com.wundergroundhw;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ForecastActivity extends AppCompatActivity implements GetDataDayWeather.IData {

    ForecastDayAdapter adapter;
    ListView listView;
    final static String POSITION_KEY = "POSITION";
    final static String DATE_KEY = "DATE";
    static DatabaseDataManager dm;

    private static String url = "http://api.wunderground.com/api/80505bfbb77ef2bd/forecast10day/q/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecastactivity);

        TextView location = (TextView) findViewById(R.id.textView_dayview);
        location.setText("Current Location:" + CityData.city_state.getCityname() + "," + CityData.city_state.getState());

        String finalurl = url +  CityData.city_state.getState() + "/" + CityData.city_state.getCityname() + ".xml";
        Log.d("url", finalurl);

    new GetDataDayWeather(this).execute(finalurl);
    }

    public void SetupData(ArrayList<ForecastDay> forecastDays){

            listView = (ListView) findViewById(R.id.listViewDay);
            adapter = new ForecastDayAdapter(getApplicationContext(),R.layout.rowitemlayout_day,forecastDays);

            listView.setAdapter(adapter);

            adapter.setNotifyOnChange(true);

            final ArrayList<ForecastDay> finalForecastDays1 = forecastDays;
            listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    ForecastDay forecastDay = (ForecastDay) listView.getItemAtPosition(position);

                    if (forecastDay.getIndicator() == 0) {
                        forecastDay.indicator = 1;
                        adapter = new ForecastDayAdapter(ForecastActivity.this,R.layout.rowitemlayout_day, finalForecastDays1);
                        adapter.setNotifyOnChange(true);
                        listView.setAdapter(adapter);
                    }
                    Intent inte = new Intent(ForecastActivity.this, AddNotesActivity.class);
                    inte.putExtra(DATE_KEY, finalForecastDays1.get(position).getDate());
                    inte.putExtra(POSITION_KEY, String.valueOf(position));
                    startActivity(inte);
                    return true;
                }
            });


        }

    @Override
    public Context getContext() {
        return this;
    }
}
