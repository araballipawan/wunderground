package hw_06.group5.com.wundergroundhw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddCityActivity extends AppCompatActivity {

    DatabaseDataManager dm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        dm = new DatabaseDataManager(this);

        Log.d("add", "add");
        final EditText editText_city = (EditText) findViewById(R.id.editText_city);
        final EditText editText_state = (EditText) findViewById(R.id.editText_state);
        Button button_save = (Button) findViewById(R.id.button_save);

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cityOriginal = editText_city.getText().toString();

                String state = editText_state.getText().toString().toUpperCase();


                if (!(GlobalVariables.allCitiesStateArrayList.contains(cityOriginal + "," + state))) {
                    editText_city.setError("Cities and State Must be Valid within the U.S.");
                } else {
                    String city = editText_city.getText().toString().replaceAll(" ", "_");
                    //new GetCurrentCondition().execute("http://api.wunderground.com/api/80505bfbb77ef2bd/conditions/q/"+state+"/"+city+".xml");
                    Cities location = new Cities(city, state,"10");
                    Log.d("location",location.toString());
//                    int size = WeatherArrayList.citiesArrayList.size();
//                    int index = 0;
//                    for (int i = 0; i <size ; i++) {
//                        if (WeatherArrayList.citiesArrayList.get(i).getState() == state && WeatherArrayList.citiesArrayList.get(i).getCity() == city){
//                            index = 1;
//                        }
//                    }
                    if (!(GlobalVariables.citiesArrayList.contains(location))) {
                        dm.saveCity(location);
                        //Log.d("pawan",WeatherArrayList.citiesArrayList.get(0).toString());
                        Intent intent = new Intent(AddCityActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        editText_city.setError("The Location is already added");
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
