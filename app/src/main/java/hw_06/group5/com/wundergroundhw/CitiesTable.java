package hw_06.group5.com.wundergroundhw;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Pawan on 3/16/2016.
 */
public class CitiesTable {

    static final String TABLE_NAME = "cities";
    static final String COLUMN_CITYKEY = "_citykey";
    static final String COLUMN_CITYNAME = "cityname";
    static final String COLUMN_STATE = "state";
    static final String COLUMN_CURRWEATHER = "currentweather";

    static public void onCreate(SQLiteDatabase db){
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("CREATE TABLE " + TABLE_NAME + " ( ");
        stringBuilder.append(COLUMN_CITYKEY + " integer primary key autoincrement , ");
        stringBuilder.append(COLUMN_CITYNAME + " text not null ,");
        stringBuilder.append(COLUMN_STATE + " text not null ,");
        stringBuilder.append(COLUMN_CURRWEATHER + " text );");

        try {
            db.execSQL(stringBuilder.toString());
        }catch (SQLException ex){
            ex.printStackTrace();
        }
    }

    static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        CitiesTable.onCreate(db);
    }
}
