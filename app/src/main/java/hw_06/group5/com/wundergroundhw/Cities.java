package hw_06.group5.com.wundergroundhw;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**

 * Created by Pawan on 3/16/2016.
 */
public class Cities implements Parcelable {
    private long _citykey;
    private String cityname;
    private String state;
    private String currWeather;

    protected Cities(Parcel in) {
        _citykey = in.readLong();
        cityname = in.readString();
        state = in.readString();
        currWeather = in.readString();
    }

    public static final Creator<Cities> CREATOR = new Creator<Cities>() {
        @Override
        public Cities createFromParcel(Parcel in) {
            return new Cities(in);
        }

        @Override
        public Cities[] newArray(int size) {
            return new Cities[size];
        }
    };

    @Override
    public String toString() {
        return "Cities{" +
                "_citykey=" + _citykey +
                ", cityname='" + cityname + '\'' +
                ", state='" + state + '\'' +
                ", currWeather='" + currWeather + '\'' +
                '}';
    }

    public long get_citykey() {
        return _citykey;
    }

    public void set_citykey(long _citykey) {
        this._citykey = _citykey;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCurrWeather() {
        return currWeather;
    }

    public void setCurrWeather(String currWeather) {
        this.currWeather = currWeather;
    }

    public Cities() {

    }

    public Cities(String cityname, String state, String currWeather) {

        this.cityname = cityname;
        this.state = state;
        this.currWeather = currWeather;
    }

    public Cities(long _citykey, String cityname, String state, String currWeather) {

        this._citykey = _citykey;
        this.cityname = cityname;
        this.state = state;
        this.currWeather = currWeather;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(_citykey);
        dest.writeString(cityname);
        dest.writeString(state);
        dest.writeString(currWeather);
    }
}
